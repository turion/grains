# Changelog

## 0.5.0-rc.0

* Return error on `push/pull` when called from a non-grain process
* Add a sink to eat messages (`Grains.Support.Sink`)
* Make `GenGrain.successors/0` and `GenGrain.predecessors/0` public. The functions return an error if called from outside of a grain.
* Remove default_grains argument from Bread creation
* Allow Grains.periodic with multiple successors

## 0.3.7

* Add `Grains.route/1,2` function that allows filtering message via pattern

## 0.3.6

* Add `GenGrain.debug_reply_chain/2` helper to debug asynchronous messages

## 0.3.5

* Add `Grains.Support.Utils` with helper functions to inject push/pull for testing.
* Add `Grains.Support.Trace` to trace push/pull messages.

## 0.3.4

* Add `Grains.get_original_recipe!/1`
* Add `Grains.Recipe.to_mermaid/1,2` to create a [mermaid](https://mermaidjs.github.io/) graph from a Recipe.

## 0.3.3

* Add pulling through `Grains.Support.Subscriber`

## 0.3.2

* Add pulling through timers
* Add `GenGrain.pull_with_tag/2,3`. This function can be used to add information to a pull operation.

## 0.3.1

* Fixes an argument forwarding bug

## 0.3.0

* Adds `Grains.Support.Cache`, a simple caching grain
* Adds `Grains.Support.Publisher`, a simple publisher grain
* Adds `Grains.Support.Subscriber`, a simple subscriber grain

## 0.2.0

* Adds `Grains.combine/2`
* Adds `Grains.get_substate/1`
* Adds `Grains.merge/2` to merge two grains. The corresponding function for `Grains.Recipe` existed since 0.1.0.
* Adds `Grains.Supervisor.which_grains/1` to retrieve the running grains at runtime
* Implements `GenServer` return values in `GenGrain`, making a grain more flexible
* Adds `Grains.Support.DebugTimer` to replace the default periodic timer for testing

## 0.1.2

* Adds setting a custom id

## 0.1.1

* Adds `GenGrain.own_full_name/0` and `GenGrain.full_name/1`

## 0.1.0

Initial version.
