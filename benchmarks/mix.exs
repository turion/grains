defmodule Grains.Benchmarks.MixProject do
  use Mix.Project

  def project do
    [
      app: :grains_benchmarks,
      version: "0.0.1",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      name: "Grains Bechmarks",
      test_coverage: [tool: Coverex.Task]
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:grains, path: "../"},
      {:benchee, "~> 1.0"}
    ]
  end
end
