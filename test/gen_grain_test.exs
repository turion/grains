defmodule Grains.GenGrainTest do
  use ExUnit.Case

  alias Grains.GenGrain

  doctest GenGrain

  @grain_name :Grain

  defmodule Grain do
    use GenGrain

    def init([reply]) do
      {:ok, reply}
    end

    def handle_call(_msg, _from, reply) do
      {:reply, reply, reply}
    end

    def handle_cast(from, reply) do
      send(from, reply)
      {:noreply, reply}
    end

    def handle_info(from, reply) do
      send(from, reply)
      {:noreply, reply}
    end
  end

  setup do
    bread = %Grains.Bread{final_recipe: [], routes: %{}}
    args = [:reply_message]
    opts = []

    {:ok, grain} = GenGrain.start_link(Grain, bread, @grain_name, args, opts)

    %{bread: bread, grain: grain}
  end

  test "GenGrain can be used as a GenServer", ctx do
    assert :reply_message = GenServer.call(ctx.grain, :send_me_a_message)

    GenServer.cast(ctx.grain, self())
    assert_receive :reply_message

    send(ctx.grain, self())
    assert_receive :reply_message
  end

  describe "GenServer return values" do
    defmodule Returner do
      use GenGrain

      def start_link(bread, args) do
        GenGrain.start_link(__MODULE__, bread, __MODULE__, args, [])
      end

      def init(return) do
        return
      end

      def handle_continue({:continue, subscriber}, state) do
        send(subscriber, :entered_continue)
        {:noreply, state}
      end

      def handle_call({return, :forward}, from, _state) do
        spawn(fn ->
          GenServer.reply(from, :ok)
        end)

        return
      end

      def handle_call(:ping, _from, state) do
        {:reply, :pong, state}
      end

      def handle_call(ret, _from, _) do
        ret
      end

      def handle_cast(ret, subscriber) do
        send(subscriber, ret)

        ret
      end

      def handle_info(:timeout, subscriber) do
        send(subscriber, :timeout)
        {:noreply, subscriber}
      end

      def handle_info(ret, subscriber) do
        send(subscriber, ret)
        ret
      end
    end

    test "init/1 -> ok", ctx do
      assert {:ok, _} = Returner.start_link(ctx.bread, {:ok, :mystate})

      assert {:ok, hibernated} = Returner.start_link(ctx.bread, {:ok, self(), 1})
      assert_receive :timeout

      assert {:ok, hibernated} = Returner.start_link(ctx.bread, {:ok, :mystate, :hibernate})
      assert :pong == GenServer.call(hibernated, :ping)

      assert {:ok, _} =
               Returner.start_link(ctx.bread, {:ok, :mystate, {:continue, {:continue, self()}}})

      assert_receive :entered_continue
    end

    test "init/1 -> stop or ignore", ctx do
      Process.flag(:trap_exit, true)
      Returner.start_link(ctx.bread, {:stop, :abort})
      assert_receive {:EXIT, _, :abort}

      Returner.start_link(ctx.bread, :ignore)
      assert_receive {:EXIT, _, :normal}
    end

    @tag :capture_log
    test "handle_call/3", ctx do
      {:ok, returner} = Returner.start_link(ctx.bread, {:ok, self()})
      state = Grains.get_substate(returner)

      #
      # reply
      #

      assert :ok = GenServer.call(returner, {:reply, :ok, state})

      assert :ok = GenServer.call(returner, {:reply, :ok, state, 1})
      assert_receive :timeout

      assert :ok = GenServer.call(returner, {:reply, :ok, state, :hibernate})
      assert :ok = GenServer.call(returner, {:reply, :ok, state})

      assert :ok =
               GenServer.call(returner, {:reply, :ok, state, {:continue, {:continue, self()}}})

      assert_receive :entered_continue

      #
      # noreply
      #
      assert :ok == GenServer.call(returner, {{:noreply, self()}, :forward})

      assert :ok == GenServer.call(returner, {{:noreply, self(), 1}, :forward})
      assert_receive :timeout

      assert :ok == GenServer.call(returner, {{:noreply, self(), :hibernate}, :forward})

      assert :ok ==
               GenServer.call(
                 returner,
                 {{:noreply, self(), {:continue, {:continue, self()}}}, :forward}
               )

      assert_receive :entered_continue
    end

    @tag :capture_log
    test "handle_cast/2", ctx do
      {:ok, returner} = Returner.start_link(ctx.bread, {:ok, self()})

      GenServer.cast(returner, m = {:noreply, self()})
      assert_receive ^m

      GenServer.cast(returner, m = {:noreply, self(), 1})
      assert_receive ^m
      assert_receive :timeout

      GenServer.cast(returner, m = {:noreply, self(), :hibernate})
      assert_receive ^m

      GenServer.cast(returner, m = {:noreply, self(), {:continue, {:continue, self()}}})
      assert_receive ^m
      assert_receive :entered_continue
    end

    @tag :capture_log
    test "handle_info/2", ctx do
      {:ok, returner} = Returner.start_link(ctx.bread, {:ok, self()})

      send(returner, m = {:noreply, self()})
      assert_receive ^m

      send(returner, m = {:noreply, self(), 1})
      assert_receive ^m
      assert_receive :timeout

      send(returner, m = {:noreply, self(), :hibernate})
      assert_receive ^m

      send(returner, m = {:noreply, self(), {:continue, {:continue, self()}}})
      assert_receive ^m
      assert_receive :entered_continue
    end
  end

  @tag :capture_log
  test "push_with_tag/1,2", ctx do
    defmodule PushWithTagReturner do
      use GenGrain

      def init(_) do
        {:ok, :ok}
      end

      def handle_pull(_from, state) do
        {:reply, :pull, state}
      end

      def handle_pull(_from, tag, state) do
        {:reply, {:pull, tag}, state}
      end
    end

    defmodule PushWithTagSubscriber do
      use Grains.GenGrain

      @impl true
      def init(args) do
        subscriber = Keyword.fetch!(args, :subscriber)
        {:ok, subscriber}
      end

      def handle_push(msg, _from, subscriber) do
        send(subscriber, msg)
        {:noreply, subscriber}
      end

      @impl true
      def handle_cast(:pull, subscriber) do
        :ok = pull()
        {:noreply, subscriber}
      end

      @impl true
      def handle_cast({:pull, predecessor}, subscriber) do
        :ok = pull(predecessor)
        {:noreply, subscriber}
      end

      @impl true
      def handle_cast({:pull_with_tag, tag}, subscriber) do
        :ok = pull_with_tag(tag)
        {:noreply, subscriber}
      end

      @impl true
      def handle_cast({:pull_with_tag, predecessor, tag}, subscriber) do
        :ok = pull_with_tag(predecessor, tag)
        {:noreply, subscriber}
      end
    end

    recipe =
      Grains.new_recipe(:TestPushWithTag, %{
        :PushWithTagReturner => :Subscriber
      })

    grains =
      Grains.new(%{
        :PushWithTagReturner => {PushWithTagReturner, [], []},
        :Subscriber => {PushWithTagSubscriber, [subscriber: self()], []}
      })

    {:ok, bread} = Grains.start_supervised(recipe, grains)
    subscriber = Grains.get_name(bread, :Subscriber)

    assert :ok == GenServer.cast(subscriber, :pull)
    assert_receive :pull

    assert :ok == GenServer.cast(subscriber, {:pull, :PushWithTagReturner})
    assert_receive :pull

    ref = make_ref()
    assert :ok == GenServer.cast(subscriber, {:pull_with_tag, ref})
    assert_receive {:pull, ^ref}

    ref = make_ref()
    assert :ok == GenServer.cast(subscriber, {:pull_with_tag, :PushWithTagReturner, ref})
    assert_receive {:pull, ^ref}
  end

  test "error on missing routes or successors" do
    # We want usable errors when push or pull is called from a non-grain process.
    assert {:error, _} = GenGrain.push(:a_successor, "msg")
    assert {:error, _} = GenGrain.push("msg")

    assert {:error, _} = GenGrain.pull()
    assert {:error, _} = GenGrain.pull(:a_predecessor)
    assert {:error, _} = GenGrain.pull_with_tag(:tag)
    assert {:error, _} = GenGrain.pull_with_tag(:a_predecessor, :tag)
  end

  test "successors and predecessors" do
    assert {:error, _} = GenGrain.predecessors()
    assert {:error, _} = GenGrain.successors()

    defmodule AGrain do
      use GenGrain

      def init(_) do
        {:ok, :ok}
      end

      def handle_call(:predecessors, _, state) do
        {:reply, GenGrain.predecessors(), state}
      end

      def handle_call(:successors, _, state) do
        {:reply, GenGrain.successors(), state}
      end
    end

    recipe =
      Grains.new_recipe(:TestPushWithTag, %{
        :FirstGrain => :SecondGrain
      })

    grains =
      Grains.new(%{
        :FirstGrain => {AGrain, [], []},
        :SecondGrain => {AGrain, [], []}
      })

    {:ok, bread} = Grains.start_supervised(recipe, grains)

    first_grain = Grains.get_name(bread, :FirstGrain)
    second_grain = Grains.get_name(bread, :SecondGrain)

    assert {:ok, []} == GenServer.call(first_grain, :predecessors)
    assert {:ok, [:FirstGrain]} == GenServer.call(second_grain, :predecessors)
    assert {:ok, [:SecondGrain]} == GenServer.call(first_grain, :successors)
    assert {:ok, []} == GenServer.call(second_grain, :successors)
  end
end
