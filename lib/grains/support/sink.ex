defmodule Grains.Support.Sink do
  @moduledoc """
  A grain to eat incoming messages without any further processing.

  This grain is meant for testing purposes, where the incoming messages
  should be ignored. This is mainly useful when a grain is needed to satisfy
  the recipe, but the messages sent to this grain are actually of no consequence.
  """

  use Grains.GenGrain

  @impl true
  def init(_args) do
    {:ok, :ok}
  end

  def handle_push(_msg, _from, state) do
    {:noreply, state}
  end
end
