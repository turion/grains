defmodule Grains.Support.Utils do
  @moduledoc """
  Small function that help with debugging and testing.
  """

  @doc """
  This function takes:
  - the grain `supervisor` pid
  - short name `receiver`
  - short name `sender`
  - a message `msg`
  It send sends a push to `receiver` with `msg` as content
  as if it came from `sender`. `receiver` needs to be an
  existing grain in the recipe.
  `sender` can be an existing grain or made up.
  """
  def inject_push(supervisor, short_receiver, short_sender, msg) do
    receiver = Grains.get_name(supervisor, short_receiver)

    case Process.whereis(receiver) do
      nil -> {:error, :unknown_receiver}
      p when is_pid(p) -> send(p, {:push, short_sender, msg})
    end
  end

  @doc """
  This function takes:
  - the grain `supervisor` pid
  - short name `receiver`
  - short name `sender`
  It send sends a pull to `receiver`
  as if it came from `sender`. `receiver` needs to be an
  existing grain in the recipe.
  `sender` can be an existing grain or made up.
  """
  def inject_pull(supervisor, short_receiver, short_sender) do
    receiver = Grains.get_name(supervisor, short_receiver)

    case Process.whereis(receiver) do
      nil -> {:error, :unknown_process_to}
      p when is_pid(p) -> send(p, {:pull, short_sender})
    end
  end

  @doc """
  This function takes:
  - the grain `supervisor` pid
  - short name `sender`
  It send sends a pull to predecessors of `sender`
  `sender` needs to be an existing grain in the recipe and alive.
  """
  def inject_pull_all(supervisor, short_sender) do
    sender = Grains.get_name(supervisor, short_sender)

    case Process.whereis(sender) do
      nil ->
        {:error, :unknown_process_to}

      p when is_pid(p) ->
        {:dictionary, dict} = Process.info(p, :dictionary)
        predecessors = Keyword.fetch!(dict, :predecessors)
        Enum.map(predecessors, fn pre -> inject_pull(supervisor, pre, short_sender) end)
    end
  end
end
