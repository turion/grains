defmodule Grains.Support.Subscriber do
  @moduledoc """
  A grain to pull message and forward pushed messages to a subscribing process.

  This grain is meant for testing purposes, where the incoming messages
  should be forwarded to the test process.
  """

  use Grains.GenGrain

  @impl true
  def init(args) do
    subscriber = Keyword.fetch!(args, :subscriber)
    {:ok, subscriber}
  end

  def handle_push(msg, _from, subscriber) do
    send(subscriber, msg)
    {:noreply, subscriber}
  end

  @impl true
  def handle_cast(:pull, subscriber) do
    pull()
    {:noreply, subscriber}
  end
end
