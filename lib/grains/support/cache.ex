defmodule Grains.Support.Cache do
  @moduledoc """
  A grain to forward messages downstream and cache the last value it received.

  This grain is meant for testing purposes, where another grain is to be tested.
  """

  use Grains.GenGrain

  @impl true
  def init(_) do
    {:ok, :no_value_received_yet}
  end

  def handle_push(msg, _from, _previous_cached_value) do
    push(msg)
    {:noreply, msg}
  end

  def handle_pull(_from, cached_value) do
    {:reply, cached_value, cached_value}
  end
end
