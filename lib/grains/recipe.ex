defmodule Grains.Recipe do
  @moduledoc """
  A Recipe describes the data flow between the processes.
  """
  defstruct [:map, :name]

  @type t :: %__MODULE__{
          name: atom,
          map: map
        }

  def new(name, map) do
    %__MODULE__{
      name: name,
      map: map
    }
  end

  @doc """
  Merges to recipes by combining all edges.
  """
  def merge(new_name, %__MODULE__{map: a}, %__MODULE__{map: b}) do
    ab = Map.merge(a, b, fn _k, v1, v2 -> Enum.uniq(List.wrap(v1) ++ List.wrap(v2)) end)
    new(new_name, ab)
  end

  @doc """
  Returns a mermaid flowchart as a string.
  """
  def to_mermaid(recipe, direction \\ "TD") do
    header = "graph #{direction}"

    body =
      recipe.map
      |> Enum.flat_map(&unfold/1)
      |> Enum.map(&to_mermaid_edge/1)

    Enum.join([header | body], " \n")
  end

  defp unfold({l, r}) when is_atom(r) do
    [{l, r, []}]
  end

  defp unfold({l, rs}) when is_list(rs) do
    Enum.flat_map(rs, fn r -> unfold({l, r}) end)
  end

  defp unfold({l, {:periodic, rs, p}}) do
    rs
    |> List.wrap()
    |> Enum.flat_map(fn r -> unfold({l, r}) end)
    |> Enum.map(fn {l, r, e} -> {l, r, [{:periodic, p} | e]} end)
  end

  defp unfold({l, {:route, _, rs, s}}) when is_list(rs) do
    rs
    |> Enum.flat_map(fn r -> unfold({l, r}) end)
    |> Enum.map(fn {l, r, e} -> {l, r, [{:route, s} | e]} end)
  end

  defp to_mermaid_edge({l, r, edge}) do
    "#{to_mermaid_node(l)} -->#{to_mermaid_edge_att(edge)} #{to_mermaid_node(r)}"
  end

  defp to_mermaid_edge_att([]) do
    ""
  end

  defp to_mermaid_edge_att(l) do
    att = fn
      {:route, s} -> "\"#{s}\""
      {:periodic, _} -> "periodic"
    end

    result = Enum.map_join(l, ", ", att)
    "|#{result}|"
  end

  defp to_mermaid_node(atom) do
    atom |> Atom.to_string() |> String.replace_leading("Elixir.", "")
  end
end
