# Debugging Message Arrival

Working with asynchronous grains can be interesting when certain optional
asynchronous behaviour needs to be observed. Consider the following example.

Lets assume that `A` pushes messages to `B`, and `B` optionally pushes a messages as a result.
How can we determine if `B` will push a message, without resorting to `receive` with an infinite
timeout? Grains provides a `Grains.debug_reply_chain(chain, payload)` function to handle this. The
function creates a message with a `payload`, which is passed along the `chain` grains. As soon as
this special message hits the last grain in that chain, the `payload` is `send()` to the original
caller of `Grains.debug_reply_chain/2`.
